package com.farukbozan.medium.memory.dump.analyse.service;

import com.farukbozan.medium.memory.dump.analyse.DumpFileLogRepository;
import com.farukbozan.medium.memory.dump.analyse.entity.DumpFileLog;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DumpFileLogService {

    private final DumpFileLogRepository dumpFileLogRepository;

    public void createLog(int logCount) {

        for (int i = 0; i < logCount; i++) {
            var newLog = buildEntity();
            dumpFileLogRepository.save(newLog);
        }

    }

    public List<DumpFileLog> getAll() {
        return dumpFileLogRepository.findAll();
    }

    private static DumpFileLog buildEntity() {
        var now = LocalDateTime.now();
        var newLog = new DumpFileLog();
        newLog.setLogDate(now);
        newLog.setLogInfo("Log created at: " + now);
        return newLog;
    }
}
