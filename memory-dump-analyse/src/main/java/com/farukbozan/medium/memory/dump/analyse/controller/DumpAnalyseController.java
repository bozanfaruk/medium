package com.farukbozan.medium.memory.dump.analyse.controller;

import com.farukbozan.medium.memory.dump.analyse.model.DumpModel;
import com.farukbozan.medium.memory.dump.analyse.service.DumpModelService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/dump")
@RequiredArgsConstructor
public class DumpAnalyseController {

    private final DumpModelService dumpModelService;

    @GetMapping("/{size}")
    public List<DumpModel> getDumpModels(@PathVariable int size) {
        return dumpModelService.getDumpModels(size);
    }

}
