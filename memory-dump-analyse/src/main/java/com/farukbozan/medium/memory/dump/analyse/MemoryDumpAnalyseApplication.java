package com.farukbozan.medium.memory.dump.analyse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MemoryDumpAnalyseApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemoryDumpAnalyseApplication.class, args);
    }

}
