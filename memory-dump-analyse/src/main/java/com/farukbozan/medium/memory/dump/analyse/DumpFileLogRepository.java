package com.farukbozan.medium.memory.dump.analyse;

import com.farukbozan.medium.memory.dump.analyse.entity.DumpFileLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DumpFileLogRepository extends JpaRepository<DumpFileLog, Long> {
}
