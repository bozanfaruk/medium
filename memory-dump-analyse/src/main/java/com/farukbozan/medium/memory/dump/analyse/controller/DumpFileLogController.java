package com.farukbozan.medium.memory.dump.analyse.controller;

import com.farukbozan.medium.memory.dump.analyse.entity.DumpFileLog;
import com.farukbozan.medium.memory.dump.analyse.service.DumpFileLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/dump-file-log")
@RequiredArgsConstructor
public class DumpFileLogController {

    private final DumpFileLogService dumpFileLogService;

    @GetMapping()
    public List<DumpFileLog> getAll() {
        return dumpFileLogService.getAll();
    }

    @GetMapping("/{count}")
    public ResponseEntity<Integer> createLog(@PathVariable int count) {
        dumpFileLogService.createLog(count);
        return ResponseEntity.ok(count);
    }

}
