package com.emlakjet.location.model;

public class LocationInfoCacheModel {

    private LocationItemCacheModel city;

    private LocationItemCacheModel district;

    private LocationItemCacheModel locality;

    private LocationItemCacheModel town;

    public LocationItemCacheModel getCity() {
        return city;
    }

    public void setCity(LocationItemCacheModel city) {
        this.city = city;
    }

    public LocationItemCacheModel getDistrict() {
        return district;
    }

    public void setDistrict(LocationItemCacheModel district) {
        this.district = district;
    }

    public LocationItemCacheModel getLocality() {
        return locality;
    }

    public void setLocality(LocationItemCacheModel locality) {
        this.locality = locality;
    }

    public LocationItemCacheModel getTown() {
        return town;
    }

    public void setTown(LocationItemCacheModel town) {
        this.town = town;
    }
}
