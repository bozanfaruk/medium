package com.farukbozan.medium.virtualthreadweb.controller;

import com.emlakjet.location.model.LocationInfoCacheModel;
import com.farukbozan.medium.virtualthreadweb.service.LocationCacheAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;
import java.util.logging.Logger;

@RestController
@RequestMapping("/virtual-thread")
public class VirtualThreadController {

    @Autowired
    private LocationCacheAdapter locationCacheAdapter;

    @GetMapping
    public Optional<LocationInfoCacheModel> getLocationInfo() throws InterruptedException {
        var locationInfo = locationCacheAdapter.getLocationInfo(10046L);
        Thread.sleep(2000);
        Logger.getLogger(VirtualThreadController.class.getName()).info("Cache read");
        return locationInfo;
    }
}
