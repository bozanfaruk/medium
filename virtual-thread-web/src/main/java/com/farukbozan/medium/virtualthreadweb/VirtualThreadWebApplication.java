package com.farukbozan.medium.virtualthreadweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VirtualThreadWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(VirtualThreadWebApplication.class, args);
    }

}
