package com.farukbozan.medium.virtualthreadweb.service;

import com.emlakjet.location.model.LocationInfoCacheModel;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LocationCacheAdapter {

    @Cacheable(cacheNames = "search-listing::location-cache", key = "{#townId}", unless = "#result = null")
    public Optional<LocationInfoCacheModel> getLocationInfo(Long townId) {

        return Optional.ofNullable(null);

    }
}
