package com.farukbozan.medium.finalize;

public class FinalizeModel {

    private int orderNumber;

    public FinalizeModel(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("finalize() called: " + orderNumber);
    }

    private static void createFinalizeModels() {
        for (int i = 0; i < 40000; i++) {
            new FinalizeModel(i);
        }
    }

    private static void printFreeMemory() {
        long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("free memory in kb: " + (freeMemory / 1024));
    }

    public static void main(String[] args) {
        printFreeMemory();

        createFinalizeModels();

        printFreeMemory();

        System.gc();

        printFreeMemory();
    }
}
