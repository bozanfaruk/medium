package com.farukbozan.medium.finalize;

import java.io.*;

public class FileWriter {

    private OutputStream os;
    private BufferedOutputStream bos;

    public FileWriter() throws FileNotFoundException {
        File file = new File("/Users/farukbozan/Desktop/finalize.txt");
        os = new FileOutputStream(file);
        bos = new BufferedOutputStream(os);
    }

    public void writeFile() throws IOException {
        bos.write("finalize".getBytes());
        bos.flush();
    }

    private void closeResource() throws IOException {
        if (os != null) {
            os.close();
            System.out.println("OutputStream closed");
        }
        if (bos != null) {
            bos.close();
            System.out.println("BufferedOutputStream closed");
        }
    }

    @Override
    protected void finalize() throws Throwable {
        closeResource();
    }

    public static void main(String[] args) throws IOException {
        FileWriter fw = new FileWriter();
        fw.writeFile();
    }
}
